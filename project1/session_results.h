#ifndef SESSION_RESULTS
#define SESSION_RESULTS

#include "constants.h"

struct date
{
	int day;
	int month;
	int year;
};

struct student
{
	char first_name[MAX_STRING_SIZE];
	char middle_name[MAX_STRING_SIZE];
	char last_name[MAX_STRING_SIZE];
};
struct mark
{
	int mk;
};
struct session_results
{
	student student1;
	date date1;
	mark mark1;
	char subject[MAX_STRING_SIZE];
};
#endif 
