#include <iostream>
#include <iomanip>

using namespace std;

#include "session_results.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(session_results* results) {
    /********** ����� �������� **********/
    cout << "�������........: ";
    // ����� �������
    cout << results->student1.last_name << " ";
    // ����� ������ ����� �����
    cout << results->student1.first_name[0] << ". ";
    // ����� ������ ����� ��������
    cout << results->student1.middle_name[0] << ".";
    cout << '\n';
    /********** ����� ���� ���������� **********/
    // ����� ����
    cout << "���� ������.....: ";
    cout << setw(4) << setfill('0') << results->date1.year << '-';
    // ����� ������
    cout << setw(2) << setfill('0') << results->date1.month << '-';
    // ����� �����
    cout << setw(2) << setfill('0') << results->date1.day;
    cout << '\n';
    /********** ����� ������� **********/
    // ����� �����
    cout << "����.....: ";
    cout << results->mark1.mk << ". ";
    // ����� �������� ��������
    cout << '"' << results->subject << '"';
    cout << '\n';
    cout << '\n';
}

int main()
{
    setlocale(0, "");
	cout << "Laboratory work#9. GIT\n";
	cout << "Variant #8. Session Results\n";
	cout << "Author: Saphranovich Dmitry\n\n";
    cout << "Group: 22_PI_POKS_1d\n";
    session_results* results[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", results, size);
        cout << "***** ���������� ������ *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(results[i]);
        }
        bool (*check_function)(session_results*) = NULL;

        cout << "\n������� ����� ��������� ���������� ������:\n";
        cout << "1) ����� ���� ��������� � �� ������ �� ���������� <<������� ��������>>\n";
        cout << "2) ����� ���� ��������� � ��������� � �������� ���� 7 ������\n";
        cout << "������� �����: ";
        int item;
        cin >> item;
        cout << "\n";
        switch (item)
        {
        case 1:
        {
            check_function = check_by_history;
            cout << "***** ���������� �� ��������� 1) *****\n\n";
            break;
        }
        case 2:
        {
            check_function = check_by_mark;
            cout << "***** ���������� �� ��������� 2) *****\n\n";
            break;
        }
        default:
        {
            throw "������!";
            break;
        }
        }
        if (check_function)
        {
            int new_size;
            session_results** filtered = filter(results, size, check_function, new_size);
            cout << "\n������� ����� �������� ����������:\n";
            cout << "1) ���������� ��������. �� ����������� ������� ��������\n";
            cout << "2) ���������� ��������. �� ����������� ������� ��������\n";
            cout << "3) ���������� ��������. �� ����������� �������� ����������, � � ������ ����� ���������� �� �������� ������\n";
            cout << "4) ���������� ��������. �� ����������� �������� ����������, � � ������ ����� ���������� �� �������� ������\n";
            cout << "������� �����: ";
            int sort;
            cin >> sort;
            cout << "\n";
            switch (sort)
            {
            case 1:
            {
                merge(filtered, new_size);
                break;
            }
            case 2:
            {
                insert(filtered, new_size);
                break;
            }
            case 3:
            {
                merge_dis(filtered, new_size);
                mark_down(filtered, new_size);
                break;
            }
            case 4:
            {
                insert_dis(filtered, new_size);
                mark_down(filtered, new_size);
                break;
            }
            default:
                throw "������!";
                break;
            }
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete results[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}