#include "filter.h"
#include <cstring>
#include <iostream>

session_results** filter(session_results* array[], int size, bool (*check)(session_results* results), int& result_size)
{
	session_results** result = new session_results * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_by_history(session_results* results)
{
	return strcmp(results->subject, "������� ��������") == 0;
}

bool check_by_mark(session_results* results)
{
	return results->mark1.mk > 7;
}

void insertion()
{

}

void merge(session_results** results, int size )
{
    int mid = size / 2;  
    if (size % 2 == 1)
        mid++;
    int h = 1;
    session_results** c = new session_results * [size];
    int step;
    while (h < size)
    {
        step = h;
        int i = 0;  
        int j = mid;   
        int k = 0;  
        while (step <= mid)
        {
            while ((i < step) && (j < size) && (j < (mid + step)))
            {    
                if (strlen(results[i]->student1.middle_name) < strlen(results[j]->student1.middle_name))
                {
                    c[k] = results[i];
                    i++; k++;
                }
                else {
                    c[k] = results[j];
                    j++; k++;
                }
            }
            while (i < step)
            {
                c[k] = results[i];
                i++; k++;
            }
            while ((j < (mid + step)) && (j < size))
            {
                c[k] = results[j];
                j++; k++;
            }
            step = step + h;
        }
        h = h * 2;
        for (i = 0; i < size; i++)
            results[i] = c[i];
    }
}

void merge_dis(session_results** results, int size)
{
    int mid = size / 2;
    if (size % 2 == 1)
        mid++;
    int h = 1;
    session_results** c = new session_results * [size];
    int step;
    while (h < size)
    {
        step = h;
        int i = 0;
        int j = mid;
        int k = 0;
        while (step <= mid)
        {
            while ((i < step) && (j < size) && (j < (mid + step)))
            {
                if (strlen(results[i]->subject) < strlen(results[j]->subject))
                {
                    c[k] = results[i];
                    i++; k++;
                }
                else {
                    c[k] = results[j];
                    j++; k++;
                }
            }
            while (i < step)
            {
                c[k] = results[i];
                i++; k++;
            }
            while ((j < (mid + step)) && (j < size))
            {
                c[k] = results[j];
                j++; k++;
            }
            step = step + h;
        }
        h = h * 2;
        for (i = 0; i < size; i++)
            results[i] = c[i];
    }
}

void mark_down(session_results** results, int size)
{
    int i = 0;
    int buf;
    while (i < size)
    {
        int count = 0;
        char* subject1 = results[i]->subject;
        for (int j = 0; j < size; j++)
        {
            if (strcmp(results[j]->subject, subject1) == 0)
                count++;
        }
        if (count == 1)
            i++;
        else
        {
            buf = i;
            session_results** c = new session_results * [count];
            for (int j = 0; j < count; j++)
            {
                c[j] = results[buf];
                buf++;

            }
            int min;
            for (int k = 0; k < count; k++)
            {
                min = k;
                for (int j = k + 1; j < count-1; j++)
                    if (c[j]->mark1.mk > c[min]->mark1.mk)
                        min = j;
                std::swap(c[k], c[min]);
            }
            for (int l = 0; l < count; l++)
            {
                results[i] = c[l];
                i++;
            }
        }
    }
}


void insert(session_results** results, int size)
{
    int i = 0;
    int j = 0;
    int k = 0;
    session_results** c = new session_results * [size];

    for (i = 0; i < size; ++i) {
        for (int j = i; j > 0 && (strlen(results[j - 1]->student1.middle_name) > strlen(results[j]->student1.middle_name)); j--) {
            c[k] = results[j - 1];
            results[j - 1] = results[j];
            results[j] = c[k];
        }
    }
}

void insert_dis(session_results** results, int size)
{
    int i = 0;
    int j = 0;
    int k = 0;
    session_results** c = new session_results * [size];

    for (i = 0; i < size; ++i) {
        for (int j = i; j > 0 && strlen(results[j - 1]->subject) > strlen(results[j]->subject); j--) {
            c[k] = results[j - 1];
            results[j - 1] = results[j];
            results[j] = c[k];
        }
    }
}