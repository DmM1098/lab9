#ifndef FILTER_H
#define FILTER_H

#include "session_results.h"

session_results** filter (session_results* array[], int size, bool (*check)(session_results* results), int& result_size);

/*
�������� ������� <function_name>:
    ������� ���������� ������ � ��������� ������� � ��� ��������� �� ��������,
    ��� ������� ������� ������ ���������� �������� true, ���������� � �����
    ������, ��������� �� ������� ������������ ��������

���������:
    array       - ������ � ��������� �������
    size        - ������ ������� � ��������� �������
    check       - ��������� �� ������� ������.
                  � �������� �������� ����� ��������� ����� �������� ���
                  ������� ������, �������� ������� ������� ����
    result_data - ��������, ������������ �� ������ - ����������, � �������
                  ������� ������� ������ ��������������� �������

������������ ��������
    ��������� �� ������ �� ���������� �� ��������, ��������������� �������
    ������ (��� ������� ������� ������ ���������� true)
*/


bool check_by_history(session_results* results);

/*
�������� ������� <check_function_name>:
    ������� ������ - ���������, ������������� �� ���� ������� ������� ������

���������:
    results - ��������� �� �������, ������� ����� ���������

������������ ��������
    true, ���� ������� ������������� ������� ������, � false � ���� ������
*/

bool check_by_mark(session_results* results);

void merge(session_results** results, int size);

void merge_dis(session_results** results, int size);

void mark_down(session_results** results, int size);

void insert(session_results** results, int size);

void insert_dis(session_results** results, int size);

#endif